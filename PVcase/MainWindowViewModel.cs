﻿using Prism.Commands;
using Prism.Mvvm;

namespace PVcase
{
    public class MainWindowViewModel : BindableBase
    {
        private const int _minColumnCount = 3;
        private const int _minRowCount = 3;

        public MainWindowViewModel()
        {
            this.ColumnsAddCommand = new DelegateCommand(ColumnsAdd);
            this.ColumnsRemoveCommand = new DelegateCommand(ColumnsRemove, CanColumnsRemove);
            this.RowsAddCommand = new DelegateCommand(RowsAdd);
            this.RowsRemoveCommand = new DelegateCommand(RowsRemove, CanRowsRemove);

            this.Rows = _minRowCount;
            this.Columns = _minColumnCount;
            this.CalculateLines();
        }

        public int Columns
        {
            get { return Frame.Columns; }
            set
            {
                Frame.Columns = value;
                this.RaisePropertyChanged();
            }
        }

        public DelegateCommand ColumnsAddCommand { get; }

        public DelegateCommand ColumnsRemoveCommand { get; }

        public double FrameHeight { get { return Frame._frameHeight; } }

        public double FrameWidth { get { return Frame._frameWidth; } }

        public Frame Frame { get; } = new Frame();

        public int Rows
        {
            get { return Frame.Rows; }
            set
            {
                Frame.Rows = value;
                this.RaisePropertyChanged();
            }
        }

        public DelegateCommand RowsAddCommand { get; }

        public DelegateCommand RowsRemoveCommand { get; }

        private void CalculateLines()
        {
            Frame.Update(Columns, Rows);
            this.RaisePropertyChanged(nameof(Frame));
        }

        private bool CanColumnsRemove()
        {
            return Frame.Columns > _minColumnCount;
        }

        private bool CanRowsRemove()
        {
            return Frame.Rows > _minRowCount;
        }

        private void ColumnsAdd()
        {
            this.Columns++;
            this.CalculateLines();
            this.ColumnsRemoveCommand.RaiseCanExecuteChanged();
        }

        private void ColumnsRemove()
        {
            if (Columns > _minColumnCount)
            {
                this.Columns--;
                this.CalculateLines();
            }
            this.ColumnsRemoveCommand.RaiseCanExecuteChanged();
        }

        private void RowsAdd()
        {
            this.Rows++;
            this.CalculateLines();
            this.RowsRemoveCommand.RaiseCanExecuteChanged();
        }

        private void RowsRemove()
        {
            if (Rows > _minRowCount)
            {
                this.Rows--;
                this.CalculateLines();
            }
            this.RowsRemoveCommand.RaiseCanExecuteChanged();
        }
    }
}
