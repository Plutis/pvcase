﻿using System.Windows;
using System.Windows.Media;

namespace PVcase
{
    public class Line
    {
        public Line(double x1, double y1, double x2, double y2, Brush color)
            : this(new Point(x1, y1), new Point(x2, y2), color)
        {
        }

        public Line(Point from, Point to, Brush color)
        {
            From = from;
            To = to;
            this.Color = color;
        }

        public Brush Color { get; }

        public Point From { get; }

        public Point To { get; }
    }
}
