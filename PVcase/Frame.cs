﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;

namespace PVcase
{
    public class Frame : ObservableCollection<Line>
    {
        public const double _frameHeight = 200;
        public const double _frameWidth = 1000;
        private const int _legHeight = 150;
        private Point _leftBottom = new Point(0, _frameHeight);
        private Point _leftTop = new Point(0, 0);
        private Point _rightBottom = new Point(_frameWidth, _frameHeight);
        private Point _rightTop = new Point(_frameWidth, 0);
        public int Columns { get; set; }
        public int Rows { get; set; }

        public void Update(int columns, int rows)
        {
            this.Columns = columns;
            this.Rows = rows;

            this.Clear();

            this.GenerateFrameBorders();

            double columnWidth = _frameWidth / Columns;
            double rowHeight = _frameHeight / Rows;
            this.GenerateLegs(columnWidth, rowHeight);

            for (int i = 1; i < Columns; i++)
            {
                double columnPosition = columnWidth * i;
                this.Add(new Line(columnPosition, 0, columnPosition, _frameHeight, Brushes.LightGray));
            }

            for (int i = 1; i < Rows; i++)
            {
                double rowPosition = rowHeight * i;
                this.Add(new Line(0, rowPosition, _frameWidth, rowPosition, Brushes.LightGray));
            }
        }

        private void GenerateFrameBorders()
        {
            this.Add(new Line(_leftTop, _rightTop, Brushes.DarkGray));
            this.Add(new Line(_rightTop, _rightBottom, Brushes.DarkGray));
            this.Add(new Line(_rightBottom, _leftBottom, Brushes.DarkGray));
            this.Add(new Line(_leftBottom, _leftTop, Brushes.DarkGray));
        }

        private void GenerateLegs(double columnWidth, double rowHeight)
        {
            this.Add(new Line(
                columnWidth,
                rowHeight,
                columnWidth + _legHeight * 1.3d,
                rowHeight + _legHeight * 1.3d,
                Brushes.Black));

            this.Add(new Line(
                columnWidth * (Columns - 1),
                rowHeight,
                (columnWidth * (Columns - 1)) + _legHeight * 1.3d,
                rowHeight + _legHeight * 1.3d,
                Brushes.Black));

            this.Add(new Line(
                columnWidth,
                rowHeight * (Rows - 1),
                columnWidth + _legHeight,
                (rowHeight * (Rows - 1)) + _legHeight,
                Brushes.Black));

            this.Add(new Line(
                columnWidth * (Columns - 1),
                rowHeight * (Rows - 1),
                (columnWidth * (Columns - 1)) + _legHeight,
                (rowHeight * (Rows - 1)) + _legHeight,
                Brushes.Black));
        }
    }
}
